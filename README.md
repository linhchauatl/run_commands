run_commands
============

Run arbitrary list of commands on a list of servers from a single machine.

Example:
```
    ruby run_commands.rb -s elasticsearch_servers.yml -c setup_newrelic.yml
```

* See the file elasticsearch_servers.yml for example about how you write server list.
* See the file reconfig_ntp.yml for example about how you list the commands.

This program will execute arbitrary commands/programs on Unix-based servers that can be accessed via SSH, with appropriate private keys on the calling machine.
