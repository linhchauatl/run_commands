
require 'optparse'
require 'rubygems'
require 'yaml'
require 'net/ssh'

DEFAULT_SERVERS = {
  'production' => {
    'portal' => {
      '1' => 'xxx.xxx.xxx.201'
    },
    
    'api' => {
      '1' => 'xxx.xxx.xxx.185',
      '2' => 'xxx.xxx.xxx.186',
      '3' => 'xxx.xxx.xxx.184'
    },
    
    'api_worker' => {
      '1' => 'xxx.xxx.xxx.221',
      '2' => 'xxx.xxx.xxx.222'
    },
    
    'cron' => {
      '1' => 'xxx.xxx.xxx.52'
    },

    'rabbitmq' => {
      '1' => '172.16.20.192'
    },

    'elasticsearch' => {
      '1' => 'xxx.xxx.xxx.198'
    }
  },
  
  'staging' => {
    'portal' => {
      '1' => 'xxx.xxx.xxx.107'
    },
    
    'api' => {
      '1' => 'xxx.xxx.xxx.220',
      '2' => 'xxx.xxx.xxx.170',
      '3' => 'xxx.xxx.xxx.171'
    },
    
    'api_worker' => {
      '1' => 'xxx.xxx.xxx.217'
    },
    
    'cron' => {
      '1' => 'xxx.xxx.xxx.218'
    },

    'rabbitmq' => {
      '1' => '172.16.20.44'
    },

    'elasticsearch' => {
      '1' => 'xxx.xxx.xxx.202'
    }
  }
}

def parse_argvs
  # Currently, only --s (servers) and --c (commands) are valid
  valid_argvs = {
    '-s' => 'servers',

    '-c' => 'commands'
  
  }
  
  options = {}
  
  opt_parser = OptionParser.new do |opts|
    valid_argvs.each do |key,value|
      opts.on(key, "--#{value} #{value}") do |arg_value|
        options[value] = arg_value
      end
    end
  end

  opt_parser.parse!(ARGV)

  options
end

OPTIONS = parse_argvs
COMMANDS_FILE = OPTIONS['commands'] || 'commands.yml'

SERVERS = (OPTIONS['servers'].to_s.size > 0)? YAML.load(File.read(File.expand_path("./", OPTIONS['servers'])))['SERVERS'] : DEFAULT_SERVERS
CONFIGS = YAML.load(File.read(File.expand_path("./", COMMANDS_FILE)))
puts "Run #{CONFIGS['description']}"
# puts "#{(`pwd`).chop}/run_command.rb"

COMMANDS = CONFIGS['commands']

SERVERS.each do |environment, server_names|
  server_names.each do |server_name, ips|
    ips.each do |number, ip|
      puts "Run on #{environment} #{server_name} #{number} #{ip}"

      ssh = Net::SSH.start(ip, 'root', {auth_methods: ['publickey'], keys: ['~/.ssh/staging', '~/.ssh/api']} )
      COMMANDS.each do |command|
        puts "$#{command}"
        result = ssh.exec!(command)
        puts result
      end

      puts("\n==================================================\n\n\n")    
    end
  end
end



 
